import * as React from 'react';
import Chart from './components/Chart';

export interface IAppProps {
}

export interface IAppState {
	value: string;
}

export default class App extends React.Component<IAppProps, IAppState> {
	constructor (props: IAppProps) {
		super(props);
		this.state = {
			value: ''
		};
	}


	public render (): JSX.Element {
		return (
			<div className="App">
				<h1>{this.state.value}</h1>
				<input onChange={this.onInputChange()}></input>
				<Chart />
			</div>
		);
	}

	private onInputChange (): ((event: React.ChangeEvent<HTMLInputElement>) => void) | undefined {
		return (event: React.ChangeEvent<HTMLInputElement>) => {
			this.setState({ value: event.target.value });
		};
	}
}

