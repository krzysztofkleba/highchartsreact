import * as React from 'react';
import ReactHighcharts from 'react-highcharts';

const config: Highcharts.Options = {
    
    // Magic starts here
    
    title: {
        text: 'My chart'
    },
    series: [{
        type: 'line',
        data: [1, 2, 3]
    }]

    // end of magic
};


export interface IChartProps {
}

export default class Chart extends React.Component<IChartProps> {
    public render () {
        return (
            <ReactHighcharts config={config}></ReactHighcharts>
        );
    }
};